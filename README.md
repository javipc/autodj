# Asistente para mezclas en MIXXX

Esta utilidad ofrece automatizaciones como sincronización de velocidad y efectos de entrada y salida de temas para mezclas manuales o con autodj.

Este programa NO DEBE USARSE CON FINES PROFESIONALES.
Fue creado para uso personal, puede contener errores.
 

Tema basado en la piel Dark Metal https://mixxx.discourse.group/t/dark-metal-skin/14096

Programa basado inicialmente en midiAutoDJ https://mixxx.discourse.group/t/auto-dj-extension-for-beatmatching-and-harmonic-mixing/15962



## Capturas

<img src="Capturas/asistente mixxx.png" alt="Captura de Pantalla" height="400">



## Videos de demostración
* Uso de efecto a la entrada y salida de temas. 
* Demostración de como conserva el valor de Pitch entre temas, incluso valores distintos de 0.

https://t.me/grupo_musica/2561


* Auto DJ mezclando un tema de 145 bpm a 76.50 bpm.
* Piel: Late Night 
* Configuración del archivo para usar el programa sin el panel de opciones.

https://t.me/grupo_musica/2564


* Limitador de velocidad en el selector de temas.
* Prueba reducir el límite para que el selector busque temas con pitch más cercano.
* Prueba de límite de 0% para seleccionar temas sin variación de pitch.

https://t.me/grupo_musica/2565


* Pitch cero: lleva el tema actual suavemente a pitch 0 (cero).

https://t.me/grupo_musica/2569



## Cambios en la piel original
* Indicadores de aguja poseen la punta blanca para generar contraste con el fondo.
* Posición del panel de onda.
* Opción para mostrar carátula en el plato giratorio.
* Cada plato posee un color diferente.
* Agregado el panel de opciones de asistencia automática para mezclas.
* Agregado el botón para llevar lentamente el pitch a cero (ver el costado del deslizador de pitch).



## Asistente
* Opción para aplicar efecto rápido en el tema siguiente, en el tema que sale o en ambos.
* Opción para aplicar efecto cuando la diferencia de pitch supere un valor determinado.
* Opción de sincronización de velocidad (Pitch) de forma lineal según la posición del mezclador.
* Capacidad de conservar la velocidad entre mezclas (Si se reproduce un tema con pitch -2 el siguiente tema tendrá pitch -2 al finalizar la mezcla).
* Capacidad para funcionar con cualquier piel configurando las variables en el archivo. 
* Cambio automático de auriculares.
* Opción para llevar el pitch a cero en cada bandeja.
* Programa reescrito basado en eventos.



## AutoDJ
* Opción para establecer el límite de diferencia velocidad.
* Permite mezclas entre BPM distantes (Ej: 140 bpm <-> 70 bpm).
* Selector de temas basado en temporizador.



## ¿Cómo funciona?

### Auto pitch
Cuando está habilitado el segundo tema que esté sonando quedará con la velocidad que tenía el primer tema, sin importar su bpm.
No requiere auto dj habilitado.

### Efectos
Ajustando la perilla de efectos de entrada y/o salida estas serán imitadas por las perillas de "efecto rápido". Cada bandeja imitará progresivamente la perilla que le corresponde dependiendo de si el tema está salindo, entrando y del punto del mezclador.
Se puede ajustar un porcentaje de pitch, si el tema siguiente supera ese valor se aplicará el efecto. Ideal para temas con mucha variación.
No requiere auto dj habilitado.

### Pitch Cero
Ajusta suavemente el pitch hasta llegar a 0 (cero).
A medida que se acerca a cero se hace más lento.

### Selector de temas
La opción pitch max permite elegir el porcentaje de velocidad máximo que debe tener un tema elegido por auto dj.
Cuando el deslizador que varía la velocidad se encuentra centrado, el porcentaje es 0%.
Cuando el deslizador se encuentra en el extremo superior o inferior el porcentaje es de 100%.
A un costado aparece una ayuda para visualizar cual es el porcentaje actual del próximo tema.
Requiere auto dj habilitado.

### Auriculares
Al finalizar la mezcla cambiará de lado la escucha en el auricular.

### Fundido
Realiza la transición de un tema a otro. Similar al de auto dj pero permite ser manipulado mientras transcurre la mezcla y también permite ser detenido.
Puede ser usado con auto dj, si es así al finalizar la mezcla cargará el siguiente tema de la lista.




## Programas usados
* KWrite
* VSCodium
* Inkscape
* Linux MX / KDE
* Simple screen recorder (para los videos)
* Mixxx (:
* git



## Aviso:
Recargar la apariencia causará mal funcionamiento del programa.

Al desactivar Auto DJ el mezclador se mueve al centro, esto puede causar cambios en la velocidad y efectos si están activos. Esto se debe a que el programa asistente no requiere AutoDJ.

Lo mismo ocurre al activar Auto DJ si el modo pitch automático está activo.

En versiones anteriores: Si un reproductor está detenido y se mueve el deslizador de mezcla de un lado a otro puede detenerse la reproducción, esto es porque cambia de lado la sincronización .

Este programa tiene un mecanismo interno para detectar el "lado" actual y el "lado" siguiente. 
Por lo tanto la dirección (izquierda / derecha) puede no coincidir con el algoritmo de auto dj de mixxx.

La búsqueda de temas puede tardar en comenzar (15 segundos), esto se debe a que usa dos temporizadores, uno de ellos es el temporizador de inactividad para ahorrar carga al procesador.



## Instalación
Apariencia:
Las instrucciones de instalación están en el archivo REARME.md.
En el caso de linux, el directorio de pieles de mi sistema (mx) es /usr/share/mixxx/skins/ 
Simplemente hay que copiar los archivos y luego en mixxx ir a opciones / interfaz / apariencia

Programa:
Las instrucciones de instalación están en el mismo archivo, lo puedes abrir con tu editor de textos favorito.



## Publicidad NO DESEADA:

Encuentra y comparte música en https://t.me/grupo_musica

Estoy en mi grupo de programación https://t.me/Programador_ES



## Más aplicaciones.
Hecha un vistazo a otros de mis proyectos favoritos y colabora.

https://gitlab.com/javipc/mas



function midiAutoDJ() {}



/* ---------------------------------------------- *
 * 
 *  Asistente MIXXX 2021 / Enero
 * 
 *  Tema basado en la piel Dark Metal https://mixxx.discourse.group/t/dark-metal-skin/14096
 * 
 *  Programa basado en midiAutoDJ https://mixxx.discourse.group/t/auto-dj-extension-for-beatmatching-and-harmonic-mixing/15962
 * 
 * 
 *  Creado por Javier Martinez
 * 
 * 
 *  Este programa NO DEBE USARSE CON FINES PROFESIONALES.
 *  Fue creado para uso personal, puede contener errores.
 * 
 *  Información en: https://github.com/mixxxdj/mixxx/wiki/Midi-Scripting
 * 
 * ---------------------------------------------- * 
 * 
 * 
 * <PUBLICIDAD tipo = "NO DESEADA>
 * 
 *  Mi gitlab está en https://gitlab.com/javipc/
 * 
 *  Encuentra y comparte música en https://t.me/grupo_musica
 * 
 *  Estoy en mi grupo de programación https://t.me/Programador_ES
 * 
 * </PUBLICIDAD>
 * 
 * 
 * ---------------------------------------------- *
 * 
 *      Este prgorama está preparado para funcionar con la piel correspondiente.
 *      Para usar este programa con otras pieles se debe cambiar las llamadas a las funciones leer () por variables o constantes.

 * 
 *      ... O modificar la función leer
 * 
 * ---------------------------------------------- *
 */


/*
	midiAutoDJ
	License: GPLv2 or GPLv3, at your discretion
	 Author: Sophia Herzog, 2016-2017

	

	Usage:
	 1. Configure script options to your liking (see below)
	 2. Start mixxx with the --developer command line option switch
		GNU/Linux example: mixxx --developer
		Windows   example: C:\Program Files\Mixxx\mixxx.exe --developer
		macOS     example: /Applications/Mixxx.app/Contents/MacOS/mixxx --developer
	 3. Navigate to Options, Preferences, Controllers, Midi Through Port
	    If the Midi Through Port is missing, make sure you started Mixxx in --developer mode.
	 4. In the Controller Setup tab: [x] Enabled
	 5. In the Scripts tab: [Add] this file, set Function Prefix to "midiAutoDJ" (without quotes)
	 6. [Apply], [OK]
	 7. Restart Mixxx
	 8. Use Auto DJ as usual

	Notes and Troubleshooting:
	 * When     using the Quick Effects fade, in Mixxx preferences, try to set Crossfader to
           Slow fade/Fast cut (additive), with  half logarithmic / scratching value
	 * When not using the Quick Effects fade, in Mixxx preferences, try to set Crossfader to
           Constant power,                with fully linear      / mixing     value
	 * If no next track is found or tracks are skipped before being properly displayed,
	   try to increase midiAutoDJ.refineDuration in Advanced Options
	 * If synchronising BPM, phase or key appears to lack sufficient time or does not find a beat,
	   try to increase Auto DJ fade duration in Mixxx (30 seconds is usually fine), and
	   try to decrease midiAutoDJ.sleepDuration in Advanced Options below.
	 * First, let Mixxx analyse your library for BPM and key
*/



// Advanced Options
midiAutoDJ.refineDuration = 3000; // Duration of sleeping between two track skips.
                                  // If Mixxx appears to hang or be overwhelmed when searching
                                  // for the next track, increase this value.
                                  // Note: Must NOT be smaller than midiAutoDJ.sleepDuration
                                  // Unit: Milliseconds; Default: 1000
midiAutoDJ.sleepDuration = 500;   // Duration of sleeping between actions.
                                  // Try to keep it low for best results.
                                  // Too low values might cause Mixxx to appear to hang.
                                  // Unit: Milliseconds; Default: 250
                                  
                                  
                                      

// Note to developers: Indent with tabs, align with spaces.
// JSHint configuration block:
/* jshint curly: true, eqeqeq: true, forin: true, freeze: true, futurehostile: true, latedef: true, nocomma: true, nonew: true, shadow: outer, singleGroups: true, strict: implied, undef: true, unused: true */
/* globals engine: false */

// Global Variables
midiAutoDJ.sleepTimer = 0; // 0 signifies a beginTimer error
midiAutoDJ.connected = 0;  // 0 signifies disconnected state
midiAutoDJ.syncing = 0;    // 1 signifies Mixxx should be trying to sync both decks
midiAutoDJ.skips = 0;      // Counts skips since last shuffle
midiAutoDJ.refineWait = 0; // Counts timer cycles since last track skip


midiAutoDJ.rateDiferencia = 0;
midiAutoDJ.mezclador = 0;
midiAutoDJ.direccion = 2; // el próximo tema


// alcance de efecto - esto es temporal
midiAutoDJ.cantidadEfecto = 0.5 ;



// Functions
midiAutoDJ.init = function(id) { // Called by Mixxx
	id = 0; // Satisfy JSHint, but keep Mixxx function signature
	engine.setValue("[Channel1]", "quantize", 1.0);
	engine.setValue("[Channel2]", "quantize", 1.0);
	engine.setValue("[Channel1]", "keylock", 1.0);
	engine.setValue("[Channel2]", "keylock", 1.0);
	engine.setValue("[Channel1]", "keylockMode", 0.0);
	engine.setValue("[Channel2]", "keylockMode", 0.0);
	// engine.setValue("[Master]", "crossfader", -1.0); // Assumes empty decks on Channel1 and Channel2; see Notes section above

	if(engine.connectControl("[AutoDJ]", "enabled", "midiAutoDJ.toggle")) {
		midiAutoDJ.connected = 1;
		engine.trigger("[AutoDJ]", "enabled");
	} else { // If connecting fails, this allows using the script anyway; least surprise.
		midiAutoDJ.sleepTimer = engine.beginTimer(midiAutoDJ.sleepDuration, "midiAutoDJ.main()");
	}
	

	if (!midiAutoDJ.sleepTimerPrograma) 	
		midiAutoDJ.sleepTimerPrograma = engine.beginTimer(midiAutoDJ.sleepDuration, "midiAutoDJ.programa()");
	
	
};

midiAutoDJ.shutdown = function(id) { // Called by Mixxx
	id = 0; // Satisfy JSHint, but keep Mixxx function signature
	if (midiAutoDJ.connected && engine.connectControl("[AutoDJ]", "enabled", "midiAutoDJ.toggle", true)) {
		midiAutoDJ.connected = 0;
	}
	if (midiAutoDJ.sleepTimer) {
		engine.stopTimer(midiAutoDJ.sleepTimer);
	}
};

midiAutoDJ.toggle = function(value, group, control) { // Called by signal connection
	group = 0;   // Satisfy JSHint, but keep Mixxx function signature
	control = 0; // Satisfy JSHint, but keep Mixxx function signature
	if (value) {
		midiAutoDJ.sleepTimer = engine.beginTimer(midiAutoDJ.sleepDuration, "midiAutoDJ.main()");
	} else if (midiAutoDJ.sleepTimer) {
		engine.stopTimer(midiAutoDJ.sleepTimer);
		midiAutoDJ.sleepTimer = 0;
	}
	
	
	if (!midiAutoDJ.sleepTimerPrograma) 
	if (value) {
		midiAutoDJ.sleepTimerPrograma = engine.beginTimer(midiAutoDJ.sleepDuration, "midiAutoDJ.programa()");
	} 
};







/*
 * 
 *      FUNCIONES
 *      Comienza la fiesta
 * 
 */






function leer (etiqueta) {    
    return engine.getValue("midiAutoDJ", etiqueta);
}

function asignar (etiqueta, dato) { 
    engine.setValue("midiAutoDJ", etiqueta, dato);
}




function direccion () {
    return midiAutoDJ.direccion;
    
    /* No se usa, puede impedir mezclar temas en la que la posición del tema siguiente sea superior a la del actual.
     * 
    var prevPos = engine.getValue("[Channel1]", "playposition");
	var nextPos = engine.getValue("[Channel2]", "playposition");
	if (prevPos < nextPos)
        return prevPos;    
    return nextPos;
    */
}

function directionDerecha () {    
	return (direccion () === 2);
}

function directionIzquierda () {
    return (direccion () === 1);
}


function diferenciaBPMabs () {
    var bpm1 = engine.getValue("[Channel1]", "file_bpm");
	var bpm2 = engine.getValue("[Channel2]", "file_bpm");    
	return Math.abs(bpm1 - bpm2);    
}


function diferenciaPitch () {
    var rate1 = engine.getValue("[Channel1]", "rate");
	var rate2 = engine.getValue("[Channel2]", "rate");
        
    return (rate1 - rate2);
}

function diferenciaPitchAbs () {
    return Math.abs (diferenciaPitch ());
}








// Recibe la posición del mezclador (crossfader) y sincroniza la velocidad de las bandejas.
function sincronizacionPitch (mezclador01) {    
    var rateDiferencia = diferenciaPitch ();     
    
    var rateObjetivo1 = midiAutoDJ.rateDiferencia + rateDiferencia * mezclador01;
    var rateObjetivo2 = midiAutoDJ.rateDiferencia - rateDiferencia * (1-mezclador01);
            
    engine.setValue("[Channel1]", "rate", rateObjetivo1);
    engine.setValue("[Channel2]", "rate", rateObjetivo2);    
}




function reiniciarEfecto (canal) {
    engine.reset ("[QuickEffectRack1_[Channel"+canal+"]]", "super1");
}

// aplica efecto en una bandeja
function efecto (canal, nivel) {
    engine.setValue("[QuickEffectRack1_[Channel"+canal+"]]", "super1", nivel);
}


// aplica efectos en las bandejas
function efectos (efecto1, efecto2) {    
    engine.setValue("[QuickEffectRack1_[Channel1]]", "super1", efecto1);
    engine.setValue("[QuickEffectRack1_[Channel2]]", "super1", efecto2);
}





function efectoSalida (mezclador01, canal) {
            // da vuelta el mezclador en el canal 2 
            if (canal == 2)
                mezclador01 = 1 - mezclador01;
            
            var rango = mezclador01 / 2;          // mezclador01 va de 0 a 1 -> rango va de 0 a 0.5;    
            var nivel = 0.5;
			
            nivel = nivel - rango;
            efecto (canal, nivel);
}


/* 
 * canal 1
 *  -> mezclador 0 : efecto = sin efecto
 *  -> mezclador 1 : efecto = completo
 * 
 * canal 2
 *  -> mezclador 0 : efecto = completo
 *  -> mezclador 1 : efecto = sin efecto
 */
 
 
function efectoEntrada (mezclador01, canal) {
            // da vuelta el mezclador en el canal 2 
            if (canal == 2)
                mezclador01 = 1 - mezclador01;
            
            var rango = mezclador01 / 2;          // mezclador01 va de 0 a 1 -> rango va de 0 a 0.5;    
            var nivel = 0.5;
			
            nivel = nivel - rango;
            efecto (canal, nivel);
}










function actualizarInterfaz () {
    
}




/*
 * 
 *      Programa Principal
 * 
 */





midiAutoDJ.programa = function  () {
    
    // Opciones de interfaz
    var maxpitch = leer ("maxpitch");
    
    maxpitch = Math.abs (maxpitch);
    asignar ("maxpitchtexto", maxpitch * 100);
    
    
    var efectopitch = leer ("efectopitch");    
    
    asignar ("efectopitchtexto", efectopitch * 100);
    
    
    // ------------------
    
    
    
    
    
    
    
    var mezclador = engine.getValue("[Master]", "crossfader"); // Oscillates between -1.0 and 1.0
    var mezclador01 = (mezclador+1.0)/2.0; // Oscillates between 0.0 and 1.0 

    
    
    
    
    if (midiAutoDJ.mezclador === mezclador) {
        
        // obtiene la velocidad actual para mantener la misma velocidad entre mezclas.
        if (mezclador === -1) 
            midiAutoDJ.rateDiferencia = engine.getValue("[Channel1]", "rate"); 
        
        if (mezclador === 1)         
            midiAutoDJ.rateDiferencia = engine.getValue("[Channel2]", "rate");
    
        // El el resto del programa se ejecuta si se está deslizando el mezclador.    
        return;
    }
    
    midiAutoDJ.mezclador = mezclador;
     
    midiAutoDJ.refineWait = 0;  
            
    
    
            
        
    // sincronizacion Pitch
    if (leer ( "sincronizacionPitch"))
        sincronizacionPitch (mezclador01);
    
        
    
	
	
	// Obtiene la diferencia de BPM.	
	// var diffBpm = diferenciaBPMabs ();
    
    var difPitch = Math.abs ( diferenciaPitch ());
	
    var efectoE   = leer ( "efectoRapidoEntrada");
    var efectoS   = leer ( "efectoRapidoSalida");
    
         
    var siguiente = direccion ();
    var anterior = 1;
    if (siguiente === 1)
        anterior = 2;
        
	
    // aplica efectos solo si hay diferencia en Pitch
        
    if (difPitch >= efectopitch) {
        if (efectoE)
            efectoEntrada (mezclador01, siguiente);
    
        if (efectoS)
            efectoSalida (mezclador01, anterior);
            
    }
        
        
    
    
    // lo último en ejecutarse, el cambio de lado.
    // si el dezlilador está del lado izquierdo el próximo tema está del lado derecho
    if (mezclador === -1) { 
        midiAutoDJ.direccion = 2;        
    }
    
    if (mezclador === 1) {
        midiAutoDJ.direccion = 1;
    }
    
    
    
    
}



















/*
 * 
 *      Programa secundario. 
 *      Para uso con AutoDJ.
 *      Es el encargado de elegir los temas con BPM cercanos.
 * 
 */


// Note: Technically, it would be cleaner to use signal connections instead of a timer.
//       However, I prefer keeping this simple; it's just a MIDI script, after all.

midiAutoDJ.main = function() { // Called by timer
    
    
    
    var mezclador = midiAutoDJ.mezclador;
    
    // apaga la sincronización antes de que termine la mezcla, sino se atora cuando carga un nuevo archivo.
    var siguiente = direccion ();
    if (siguiente === 1)
        if (mezclador < -0.75) 
            engine.reset("[Channel2]", "sync_enabled");
        
        
    if (siguiente === 2)
        if (mezclador > 0.75) 
            engine.reset("[Channel1]", "sync_enabled");
            

        
        
    // Si el mezclador NO está en los extremos... sale.
    // Entonces el código a continuación se ejecuta únicamente con el mezclador en los extremos.
    if (mezclador > -1)
        if (mezclador < 1)
            return;
        
    
    midiAutoDJ.refineWait++;
    if ( midiAutoDJ.refineWait*midiAutoDJ.sleepDuration < midiAutoDJ.refineDuration )         
        return;    
    midiAutoDJ.refineWait = 0;
    
    
    
    
    
    
    
        
    
    
    
    
    
    /*
     *  Este código funciona para seleccionar temas por BPM
     *    
        
    // Obtiene la diferencia de BPM.	
	var diffBpm = diferenciaBPMabs ();
		
    
    var maxbpm = leer ("sincronizacionbpm");
    // Selecciona un tema con BPM cercano.

    
    if ( diffBpm > maxbpm) {
            
        if (mezclador < 0) 
            engine.reset("[Channel2]", "sync_enabled");
        else
            engine.reset("[Channel1]", "sync_enabled");
        
        engine.setValue("[AutoDJ]", "skip_next", 1.0);
        engine.reset("[AutoDJ]", "skip_next"); // Have to reset manually        
        return;
    }
    */
    
    
    
    
    
    var anterior = 1;
    if (siguiente === 1)
        anterior = 2;
	
    // Si la sincronización está apagada la activa y sale para que el sistema calcule el pitch.    

	if (! engine.getValue ("[Channel"+siguiente+"]", "sync_enabled")) {
        engine.setValue("[Channel"+siguiente+"]", "sync_mode", 1.0);
        engine.setValue("[Channel"+anterior+"]", "sync_mode", 2.0);    
        engine.setValue("[Channel"+siguiente+"]", "sync_enabled", 1.0);
        return;
    }
    
    
    
    // Si la sincronización está encendida (condición anterior) compara la diferencia de pitch.

    
    var maxpitch = leer ("maxpitch");

    
    var difPitch = Math.abs ( diferenciaPitch ());
    
    asignar ("pitchsiguientetexto", difPitch * 100);
    
    
    if ( difPitch > maxpitch) {
            
        // apaga la sincronización
        engine.reset("[Channel"+siguiente+"]", "sync_enabled");
        
        // Reinicia el deslizador de pitch
        engine.reset("[Channel"+siguiente+"]", "rate");
        
        // pasa al siguiente tema
        engine.setValue("[AutoDJ]", "skip_next", 1.0);
        engine.reset("[AutoDJ]", "skip_next"); // Have to reset manually        
        return;
    }

    
    // En este punto existe un tema seleccionado. ----------------------------
    
    
    
    // Reinicia las perillas de efectos, si se usa efectos.
    if (leer ( "efectoRapidoEntrada") || leer ( "efectoRapidoSalida"))    
        engine.reset("[QuickEffectRack1_[Channel"+siguiente+"]]", "super1");
    
    
    
    
    
    //engine.setValue("[Channel"+next+"]", "sync_enabled", 0.0);
    
    
    
     
    
};
	




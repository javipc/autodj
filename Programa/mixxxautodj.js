function midiAutoDJ() {}



/* ---------------------------------------------- *
 * 
 *  Asistente MIXXX 2021 / Enero / 12
 * 
 *  Creado por Javier Martinez
 * 
 *  Tema basado en la apariencia Dark Metal https://mixxx.discourse.group/t/dark-metal-skin/14096
 * 
 *  Programa inicialmente basado en midiAutoDJ https://mixxx.discourse.group/t/auto-dj-extension-for-beatmatching-and-harmonic-mixing/15962
 * 
 *  Encuentra la última versión en https://gitlab.com/javipc/autodj
 *  También encontrarás más información en el archivo README.md
 * 
 * 
 *  Este programa NO DEBE USARSE CON FINES PROFESIONALES.
 *  Fue creado para uso personal, puede contener errores.
 * 
 *  Información en: https://github.com/mixxxdj/mixxx/wiki/Midi-Scripting
 * 
 * ---------------------------------------------- * 
 * 
 * 
 * <PUBLICIDAD tipo = "NO DESEADA>
 * 
 *  Encuentra y comparte música en https://t.me/grupo_musica
 * 
 *  Estoy en mi grupo de programación https://t.me/Programador_ES
 * 
 * </PUBLICIDAD>
 * 
 * 
 * ---------------------------------------------- *
 * 
 *      Este programa está preparado para funcionar con la piel correspondiente.
 *      Para usar este programa con otras pieles se debe cambiar 
 * 
 *          midiAutoDJ.conf_piel = true;
 * 
 *      por
 * 
 *          midiAutoDJ.conf_piel = false;
 * 
 *      Luego configurar los valores deseados en este archivo.
 * 
 * 
 * ---------------------------------------------- *
 */


/*
	midiAutoDJ
	License: GPLv2 or GPLv3, at your discretion
	 Author: Sophia Herzog, 2016-2017

	

	Usage:
	 1. Configure script options to your liking (see below)
	 2. Start mixxx with the --developer command line option switch
		GNU/Linux example: mixxx --developer
		Windows   example: C:\Program Files\Mixxx\mixxx.exe --developer
		macOS     example: /Applications/Mixxx.app/Contents/MacOS/mixxx --developer
	 3. Navigate to Options, Preferences, Controllers, Midi Through Port
	    If the Midi Through Port is missing, make sure you started Mixxx in --developer mode.
	 4. In the Controller Setup tab: [x] Enabled
	 5. In the Scripts tab: [Add] this file, set Function Prefix to "midiAutoDJ" (without quotes)
	 6. [Apply], [OK]
	 7. Restart Mixxx
	 8. Use Auto DJ as usual

	Notes and Troubleshooting:
	 * When     using the Quick Effects fade, in Mixxx preferences, try to set Crossfader to
           Slow fade/Fast cut (additive), with  half logarithmic / scratching value
	 * When not using the Quick Effects fade, in Mixxx preferences, try to set Crossfader to
           Constant power,                with fully linear      / mixing     value	
	 * First, let Mixxx analyse your library for BPM and key
*/




                             
                                  
                                      

// Note to developers: Indent with tabs, align with spaces.
// JSHint configuration block:
/* jshint curly: true, eqeqeq: true, forin: true, freeze: true, futurehostile: true, latedef: true, nocomma: true, nonew: true, shadow: outer, singleGroups: true, strict: implied, undef: true, unused: true */
/* globals engine: false */












// Configuración --------------------------

// true = obtiene datos de la interfaz
// false = Obtiene los datos de la configuración siguiente...
midiAutoDJ.conf_piel = true; 

// Si se usa una piel diferente y el parametro anterior está en "false"
// se usará la siguiente configuración...
midiAutoDJ.conf_efectosActivos  = 1     // 0 = desactivado
midiAutoDJ.conf_efectoEntrada   = 0.5 ; // 
midiAutoDJ.conf_efectoSalida    = 0.25 ; // 
midiAutoDJ.conf_efectoPitch     = 0.25;   // 0 = desactivado
midiAutoDJ.conf_autoPitch       = 1;        // 0 = desactivado
midiAutoDJ.conf_pitchMax        = 0.75;
midiAutoDJ.conf_auriculares     = 1;






// Uso interno -------------------------------
midiAutoDJ.rateDiferenciaCanales = 0;  // = (actual - siguiente)
midiAutoDJ.rateDiferenciaCero    = 0;  // posición inicial del canal

midiAutoDJ.mezclador = 0;           // posición actual del mezclador
midiAutoDJ.direccion = 2;           // el próximo tema
midiAutoDJ.actual    = 1;              // tema actual
midiAutoDJ.mezclando = false;

midiAutoDJ.objetoPitchCero = 0;
midiAutoDJ.objetoMezclador = 0;
midiAutoDJ.objetoSelector  = 0;

midiAutoDJ.TemporizadorBusquedaDeTemas = 5000;   // Tiempo entre busquedas de temas 
midiAutoDJ.TemporizadorPitchCero = 250;   // Tiempo entre busquedas de temas 








/*
    Manejador de temporizadores
*/


function iniciar (programa, duracion) {
    return engine.beginTimer(duracion, programa);
}

function detener (puntero) {
    if (puntero)
        engine.stopTimer(puntero);
}

function iniciarSelectorDeTemas () {
    if (midiAutoDJ.objetoSelector == 0)
        midiAutoDJ.objetoSelector = iniciar ("revisarNuevoTema()", midiAutoDJ.TemporizadorBusquedaDeTemas); 
}

function detenerSelectorDeTemas () {
    detener (midiAutoDJ.objetoSelector);
    midiAutoDJ.objetoSelector = 0;
}


function iniciarPitchCero () {
    if (midiAutoDJ.objetoPitchCero == 0)
        midiAutoDJ.objetoPitchCero = iniciar ("pitchCeroTemporizador()", midiAutoDJ.TemporizadorPitchCero); 
}

function detenerPitchCero () {
    detener (midiAutoDJ.objetoPitchCero);
    midiAutoDJ.objetoPitchCero = 0;
}



/*
    Creador de eventos
*/

function evento (grupo, control, funcion) {
    if(engine.connectControl(grupo, control, funcion)) { 
        engine.trigger(grupo, control);
        return true;
    }
    return false;
}






// Funciones
midiAutoDJ.init = function(id) { // Called by Mixxx
	id = 0; // Satisfy JSHint, but keep Mixxx function signature
	engine.setValue("[Channel1]", "quantize", 1.0);
	engine.setValue("[Channel2]", "quantize", 1.0);
	engine.setValue("[Channel1]", "keylock", 1.0);
	engine.setValue("[Channel2]", "keylock", 1.0);
	engine.setValue("[Channel1]", "keylockMode", 0.0);
	engine.setValue("[Channel2]", "keylockMode", 0.0);
	


        
    evento ("[Master]", "crossfader", "midiAutoDJ.programaMezclador");
    
    // evento ("[Channel1]", "rate", "midiAutoDJ.programaVelocidad");
    // evento ("[Channel2]", "rate", "midiAutoDJ.programaVelocidad");

    evento ("[AutoDJ]", "enabled", "midiAutoDJ.programaSelectorEv");
    evento ("[Channel1]", "track_loaded", "midiAutoDJ.programaSelectorEv");
    evento ("[Channel2]", "track_loaded", "midiAutoDJ.programaSelectorEv");


    evento ("midiAutoDJ",  "pitchCero1", "midiAutoDJ.programaPitchCeroEv");
    evento ("midiAutoDJ",  "pitchCero2", "midiAutoDJ.programaPitchCeroEv");
    evento ("midiAutoDJ",  "pitchCero3", "midiAutoDJ.programaPitchCeroEv");
    evento ("midiAutoDJ",  "pitchCero4", "midiAutoDJ.programaPitchCeroEv");
    
    if (midiAutoDJ.conf_piel ) {

        evento ("midiAutoDJ",  "efectoRapido", "midiAutoDJ.programaInterfaz") ;    
        evento ("midiAutoDJ",  "efectoRapidoEntrada", "midiAutoDJ.programaInterfaz") ;
        evento ("midiAutoDJ",  "efectoRapidoSalida", "midiAutoDJ.programaInterfaz") ;
        evento ("midiAutoDJ",  "efectopitch", "midiAutoDJ.programaInterfaz");
        evento ("midiAutoDJ",  "sincronizacionPitch", "midiAutoDJ.programaInterfaz");
        evento ("midiAutoDJ",  "maxpitch", "midiAutoDJ.programaInterfaz");        
        evento ("midiAutoDJ",  "auriculares", "midiAutoDJ.programaInterfaz");
        evento ("midiAutoDJ",  "mezclador", "midiAutoDJ.programaInterfaz");
        

    }

    //evento ("[Channel1]", "track_loaded", "midiAutoDJ.prueba");
        
};


midiAutoDJ.prueba = function  (valor, grupo, control) {
    engine.reset ("[QuickEffectRack1_[Channel1]]", "super1");
}






















/*
 * 
 *      FUNCIONES
 *      Comienza la fiesta
 * 
 */





 function clic (grupo, parametro) {
    engine.setValue(grupo, parametro, 1.0);
    engine.reset   (grupo, parametro); 
 }



/*
    Datos de la interfaz
*/

function leer (etiqueta) {    
    return engine.getValue("midiAutoDJ", etiqueta);
}

function asignar (etiqueta, dato) { 
    engine.setValue("midiAutoDJ", etiqueta, dato);
}




/*
    Próximo tema
*/


function direccion () {
    return midiAutoDJ.direccion;
}



function direccionBasadaEnPosicion () {
    /* 
        No se usa mientras se mezcla, puede impedir mezclar temas en la que la posición del tema siguiente sea superior a la del actual.
        Se usa cuando se cargan temas
     */ 

    if ( engine.getValue("[Channel1]", "stop") )
        return 1
    
    if (engine.getValue("[Channel2]", "stop"))
        return 2

    if ( engine.getValue("[Channel1]", "playposition") != 0.5)
    if ( engine.getValue("[Channel2]", "playposition") != 0.5)    
	if ( engine.getValue("[Channel1]", "playposition") < engine.getValue("[Channel2]", "playposition"))
        return 1; 

    return 2;    
}




/*
    Diferencia entre temas.
*/

function diferenciaBPMabs () {
    var bpm1 = engine.getValue("[Channel1]", "file_bpm");
	var bpm2 = engine.getValue("[Channel2]", "file_bpm");    
	return Math.abs(bpm1 - bpm2);    
}


function diferenciaPitchCalcular () {
    var rate1 = engine.getValue("[Channel1]", "rate");
	var rate2 = engine.getValue("[Channel2]", "rate"); 
    //return (rate1 - rate2);
    midiAutoDJ.rateDiferenciaCanales =  (rate1 - rate2);
}

function diferenciaPitch () {
    return midiAutoDJ.rateDiferenciaCanales;
}

function diferenciaPitchAbs () {
    return Math.abs (diferenciaPitch ());
}











// Recibe la posición del mezclador (crossfader) y sincroniza la velocidad de las bandejas.
function sincronizacionPitch (mezclador01) { 
    
    var rateDiferencia = diferenciaPitch ();     
    
    var rateObjetivo1 = midiAutoDJ.rateDiferenciaCero + (rateDiferencia * mezclador01);
    var rateObjetivo2 = midiAutoDJ.rateDiferenciaCero - (rateDiferencia * (1-mezclador01));
    
    if (direccion () == 1) {        
        engine.setValue("[Channel2]", "rate", rateObjetivo2); 
        engine.setValue("[Channel1]", "rate", rateObjetivo1);
    } else {
        engine.setValue("[Channel1]", "rate", rateObjetivo1);
        engine.setValue("[Channel2]", "rate", rateObjetivo2);
    } 
}






/*
    Efectos
*/

function reiniciarEfecto (canal) {
    engine.reset ("[QuickEffectRack1_[Channel"+canal+"]]", "super1");
}

// aplica efecto en una bandeja
function efecto (canal, nivel) {
    engine.setValue("[QuickEffectRack1_[Channel"+canal+"]]", "super1", nivel);
}


function efectoSalida (mezclador01, canal) {
            
            // da vuelta el mezclador en el canal 2 
            if (canal == 2) mezclador01 = 1 - mezclador01;
                        
            var objetivo = midiAutoDJ.conf_efectoSalida ;
            var diferencia = 0.5 - objetivo;
            var nivel = 0.5;
			
            nivel = nivel - (diferencia * mezclador01);
            efecto (canal, nivel);
}


/* 
 * canal 1
 *  -> mezclador 0 : efecto = sin efecto
 *  -> mezclador 1 : efecto = completo
 * 
 * canal 2
 *  -> mezclador 0 : efecto = completo
 *  -> mezclador 1 : efecto = sin efecto
 */
 
 
function efectoEntrada (mezclador01, canal) {
            // da vuelta el mezclador en el canal 2 
            if (canal == 2) mezclador01 = 1 - mezclador01;
                        
            var objetivo = midiAutoDJ.conf_efectoEntrada ;
            var diferencia = 0.5 - objetivo;
            var nivel = 0.5;
			
            nivel = nivel - (diferencia * mezclador01);
            efecto (canal, nivel);
}











/*
    Sincronización de MIXXX
    
*/

function apagarSincronizacion () {        
    engine.reset("[Channel1]", "sync_enabled"); 
    engine.reset("[Channel2]", "sync_enabled"); 
}



function encenderSincronizacion () {
    

    var siguiente = direccion ();
    anterior = 1;
    if (siguiente == 1)
        anterior = 2;

    

    // quita los modos
    // engine.setValue("[Channel"+siguiente+"]", "sync_mode", 0.0);
    // engine.setValue("[Channel"+anterior +"]", "sync_mode", 0.0);

    // aplica modos
    // engine.setValue("[Channel"+siguiente+"]", "sync_mode", 1.0);
    // engine.setValue("[Channel"+anterior +"]", "sync_mode", 2.0);  

    
    // clic para sincronizar rápido
    // apaga y enciende la sincronización
    engine.reset("[Channel"+siguiente+"]", "sync_enabled");
    engine.setValue("[Channel"+siguiente+"]", "sync_enabled", 1.0);    
        
    engine.setValue("[Channel"+anterior +"]", "sync_enabled", 1.0);
    
    actualizarVelocidad ();
    
}







/*
    Cambio de lado
*/



function cambiarLado () {


    var siguiente = direccionBasadaEnPosicion ();    
    midiAutoDJ.direccion = siguiente;   
    
    // Reinicia las perillas de efectos, si se usa efectos.
    if (midiAutoDJ.conf_efectosActivos)
        engine.reset("[QuickEffectRack1_[Channel"+siguiente+"]]", "super1");

    

    // Hace el cambio de auriculares
    if (midiAutoDJ.conf_auriculares) {
        engine.reset("[Channel1]","pfl");
        engine.reset("[Channel2]","pfl");
        engine.setValue("[Channel"+siguiente+"]","pfl", 1);
        
    }
    

    

    // Opción de mezclador automático 
    if (leer ("mezclador"))
        if (engine.getValue ("[AutoDJ]", "enabled")) {
            
            // clic ("[Channel"+siguiente+"]","play");            
            engine.setValue ("[Channel"+siguiente+"]","stop", 1);
            clic ("[Channel"+siguiente+"]","eject");
            clic ("[AutoDJ]", "skip_next"); 
        }

    // Apaga el mezclador automático.
    asignar ("mezclador", 0);

    midiAutoDJ.mezclando = false;
}

















/*
 * 
 *      Programas Principales
 * 
 */





 



 // Realiza la mezcla
 
 midiAutoDJ.programaFundido = function () {

    

     //var mezclador = engine.getValue("[Master]", "crossfader"); // Oscillates between -1.0 and 1.0
    
    if (direccion () == 1)
        engine.setValue("[Master]", "crossfader", midiAutoDJ.mezclador - 0.02);
    else
        engine.setValue("[Master]", "crossfader", midiAutoDJ.mezclador + 0.02);

    var mezclador = engine.getValue ("[Master]", "crossfader");

    if (mezclador == 1) 
    if (direccion () == 2)
        cambiarTema     (1);
    
    if (mezclador == -1) 
    if (direccion () == 1)        
        cambiarTema     (2);
    
 }





 // Expulsa el tema y pasa al siguiente

function cambiarTema (siguiente) {

    engine.setValue ("[Channel"+siguiente+"]","stop", 1);
        
    if (leer ("mezclador")) {
        asignar ("mezclador", 0);
        clic ("[Channel"+siguiente+"]","eject");
    }
        
    if (engine.getValue ("[AutoDJ]", "enabled")) {        
        clic ("[AutoDJ]", "skip_next"); 
    }
}






/*
    lectura de todas las variables que ofrece la pantalla
*/

midiAutoDJ.programaInterfaz = function (valor, grupo, control)  {

        
    midiAutoDJ.conf_efectoEntrada = leer ( "efectoRapidoEntrada") ; // 0.5 = desactivado
    midiAutoDJ.conf_efectoSalida = leer ( "efectoRapidoSalida") ; // 0.5 = desactivado
    midiAutoDJ.conf_efectoPitch =  leer ("efectopitch"); 
    midiAutoDJ.conf_autoPitch = leer ( "sincronizacionPitch");
    midiAutoDJ.conf_pitchMax = leer ("maxpitch");
    midiAutoDJ.conf_efectosActivos = leer ("efectoRapido");
    midiAutoDJ.conf_auriculares = leer ("auriculares");
    midiAutoDJ.conf_pitchMax = leer ("maxpitch");
    


    // envia datos a la pantalla.
    asignar ("maxpitchtexto", midiAutoDJ.conf_pitchMax * 100);
    asignar ("efectopitchtexto", midiAutoDJ.conf_efectoPitch * 100);
    asignar ("maxpitchtexto", midiAutoDJ.conf_pitchMax * 100);


    if (midiAutoDJ.conf_efectosActivos == 0) {
        if (midiAutoDJ.conf_efectoEntrada != 0.5)
            asignar ("efectoRapidoEntrada", 0.5);

        if (midiAutoDJ.conf_efectoSalida != 0.5)
            asignar ("efectoRapidoSalida", 0.5);
    }



    
    if (leer ("mezclador") == 1)
        if (midiAutoDJ.objetoMezclador == 0)  {
            // [Channel1],play
            // [Channel2],play
            engine.setValue ("[Channel1]","play", 1);
            engine.setValue ("[Channel2]","play", 1);
            midiAutoDJ.objetoMezclador = iniciar ("midiAutoDJ.programaFundido()", 200);
        }

    if (leer ("mezclador") == 0) {
        detener (midiAutoDJ.objetoMezclador);
        midiAutoDJ.objetoMezclador = 0;
    }
     


}








/*
    Evento:
    Obtiene el dato de velocidad que indica la perilla
*/

midiAutoDJ.programaVelocidad = function (valor, grupo, control)  { }

function actualizarVelocidad () {
    
    var siguiente = direccion ();


    // arranque de la mezcla

    if (siguiente ==  2)
    if (midiAutoDJ.mezclador == -1) {
        midiAutoDJ.rateDiferenciaCero = engine.getValue ("[Channel1]", "rate");
        diferenciaPitchCalcular ();
    }
            

    if (siguiente ==  1)
    if (midiAutoDJ.mezclador == 1) {
        midiAutoDJ.rateDiferenciaCero = engine.getValue ("[Channel2]", "rate");
        diferenciaPitchCalcular ();
    }
                   
    
}








/*
  
    Evento:
    Al moverse el mezclador realiza las tareas de sincroización de pich y efectos.

*/




midiAutoDJ.programaMezclador = function  (valor, grupo, control) {
    
    
        
        
    midiAutoDJ.existeEventoMezcla = true;

    //var mezclador = engine.getValue("[Master]", "crossfader"); // Oscillates between -1.0 and 1.0
    var mezclador = valor;

    // previene los saltos al activar / desactivar Auto DJ
    var salto = (Math.abs (mezclador - midiAutoDJ.mezclador) == 1);
    
    // valores de mezclador entre 0 y 1
    var mezclador01 = (mezclador+1.0)/2.0; // Oscillates between 0.0 and 1.0 

    var siguiente = direccion ();
    var anterior = 1;
    if (siguiente === 1)
        anterior = 2;


        /*
    // ANTES DE ACTUALIZAR LA VARIABLE midiAutoDJ.mezclador
    // verifica si su valor anterior era una esquina. Solo así enciende la sincronización.

    
                */

       
    // inicio de la mezcla
    if (midiAutoDJ.mezclador == 1)
    if (siguiente ==  1)        
        encenderSincronizacion ();

    if (midiAutoDJ.mezclador == -1)
    if (siguiente ==  2)        
        encenderSincronizacion ();

    
    if (midiAutoDJ.mezclador == 1 || midiAutoDJ.mezclador == -1 )
        if (midiAutoDJ.conf_autoPitch)
            actualizarVelocidad ();


    
    // Ahora sí actualiza el valor de mezclador
    midiAutoDJ.mezclador = mezclador;
     
    
    
            
    midiAutoDJ.mezclando = true;
    

    
                   
    
    

        
    // sincronización Pitch
    if (salto == false)
    if (midiAutoDJ.conf_autoPitch)                
        sincronizacionPitch (mezclador01);
    
        
    
	
	
	// Obtiene la diferencia de BPM.	
	// var diffBpm = diferenciaBPMabs ();
    
    
	
    
    // aplica efectos si está activo.
    if (salto == false)
    if (midiAutoDJ.conf_efectosActivos) 

        // aplica efectos solo si hay diferencia en Pitch
        if (diferenciaPitchAbs () >= midiAutoDJ.conf_efectoPitch) {
            if (midiAutoDJ.conf_efectoEntrada != 0.5)
                efectoEntrada (mezclador01, siguiente);
        
            if (midiAutoDJ.conf_efectoSalida != 0.5)
                efectoSalida (mezclador01, anterior);
                
        }
            
    
        
    
    
    // lo último en ejecutarse
  


    // fin de la mezcla
    
    if (midiAutoDJ.mezclador == 1)
    if (siguiente ==  2)
            apagarSincronizacion ();
            

    if (midiAutoDJ.mezclador == -1)
    if (siguiente ==  1)
            apagarSincronizacion ();
            
    
}



















/*
 * 
 *      Programa selector. 
 *      Para uso con AutoDJ.
 *      Es el encargado de elegir los temas con BPM cercanos.
 * 
 */








/*
     Receptor de evento:
     Recibe un evento (nuevo tema, activación autodj)
     Llama a la función para revisar nuevos temas cargados.


*/

midiAutoDJ.programaSelectorEv = function  (valor, grupo, control) {
    // apagarSincronizacion ();
    
    if (grupo == "[AutoDJ]")
        if (valor == false) {
            detenerSelectorDeTemas ();
            return;
        }

    
    cambiarLado ();
    
    iniciarSelectorDeTemas();  
}








/*
    Procesa un nuevo tema cargado en la bandeja.
    Aprovecha el análisis de mixxx para buscar temas con pitch cercanos.
    Requiere autopitch.
    No requiere auto dj.
    Puede ser usado por temporizadores O eventos (no por ambos).
 */

function revisarNuevoTema () {

    // Es solo para autopich    
    if (midiAutoDJ.conf_autoPitch == 0) 
        return;
    


    // al iniciar auto dj ambas bandejas pueden estar vacias.
    // se debe comprobar que al menos una tenga una archivo cargado.
    if (engine.getValue("[Channel1]", "track_loaded") == 0)
        return;
    if (engine.getValue("[Channel2]", "track_loaded") == 0)
        return;




    

    var siguiente = direccionBasadaEnPosicion ();
    midiAutoDJ.direccion = siguiente;   
    
    if (siguiente == 1)
        midiAutoDJ.actual = 2
    else
        midiAutoDJ.actual = 1;  
    
        


    // Si la sincronización está apagada la activa y sale para que el sistema calcule el pitch.    

	if (! engine.getValue ("[Channel"+siguiente+"]", "sync_enabled")) {                
        encenderSincronizacion ()

        // En este punto puede no haber infomación, pero si la hay la muestra.        
        asignar ("pitchsiguientetexto", diferenciaPitchAbs () * 100);
        return;
    }
    
    
    
    // Si la sincronización está encendida (condición anterior) compara la diferencia de pitch.

    var difPitch =  diferenciaPitchAbs ();    
    asignar ("pitchsiguientetexto", difPitch * 100);
    
    
    // este código es solo para auto dj
    // pasa al siguiente tema si supera el máximo pitch.
    if (engine.getValue ("[AutoDJ]", "enabled")) 
    if ( difPitch > midiAutoDJ.conf_pitchMax) {
            
        // apaga la sincronización        
        apagarSincronizacion ();
        
        // Reinicia el deslizador de pitch
        engine.reset("[Channel"+siguiente+"]", "rate");
        
        // pasa al siguiente tema        
        clic ("[AutoDJ]", "skip_next");         

        return;
    }

    
    // En este punto existe un tema seleccionado. ----------------------------
    

    // El selector de temas debe quedar de fondo para que observe cambios en la perilla maxpitch
    // if (engine.getValue ("[AutoDJ]", "enabled") == false)  detenerSelectorDeTemas ();
    
    // Reinicia las perillas de efectos, si se usa efectos.
    // if (midiAutoDJ.conf_efectosActivos) engine.reset("[QuickEffectRack1_[Channel"+siguiente+"]]", "super1");


}




	



 // Lleva el nivel de velocidad a cero.

 midiAutoDJ.programaPitchCeroEv = function  (valor, grupo, control)  {
     if (valor)
        iniciarPitchCero ();
 }

 function pitchCeroTemporizador () {
     var detener = true;
     for (var x = 1; x <= 4; x++) {
        if (leer ("pitchCero" + x)) {
            pitchCero (x);
            detener = false;
        }
     }
     if (detener)
        detenerPitchCero ();
 }

 function pitchCero (canal) {
     
    
    var velocidad = engine.getValue("[Channel"+canal+"]", "rate");
    var adaptador = Math.abs (velocidad / 50);

    if (velocidad > 0)
        velocidad = velocidad - adaptador;
    if (velocidad < 0)
        velocidad = velocidad + adaptador;

    if (velocidad > -0.01) 
        if (velocidad < 0.01)
            velocidad = 0;
    engine.setValue("[Channel"+canal+"]", "rate", velocidad);
    
    if (velocidad == 0) 
        asignar ("pitchCero" + canal, 0);
    
 }


